
#include "Incubator.h"
#include <HardwareSerial.h>
#include <DHT.h>

extern HardwareSerial Serial;

Incubator* _instance = NULL;

Incubator* Incubator::getInstance(){
	if ( _instance == NULL ){
		_instance = new Incubator();
	}

	return _instance;
}

void task_broedmachine_run(Task* self){
	_instance->run();
}
void task_broedmachine_run_ki(Task* self){
	_instance->run_ki(self);
}
void task_broedmachine_run_dht(Task* self){
	_instance->run_dth();
}


void Incubator::init(Relay* relay_heating, Relay* relay_fan, Relay* relay_light, Relay* relay_power, Relay* relay_direction, uint8_t up_sensor, uint8_t down_sensor, uint8_t door_sensor, DHT* dht, Sheduler* sheduler){

	r_heating      = relay_heating;
	r_fan          = relay_fan;
	r_light        = relay_light;

	r_ki_power     = relay_power;
	r_ki_direction = relay_direction;
	i_ki_up        = up_sensor;
	i_ki_down      = down_sensor;

	i_door_open    = door_sensor;

	dhtsensor      = dht;

	pinMode( i_ki_up    , INPUT ); digitalWrite(i_ki_up    , HIGH);
	pinMode( i_ki_down  , INPUT ); digitalWrite(i_ki_down  , HIGH);
	pinMode( door_sensor, INPUT ); digitalWrite(door_sensor, HIGH);

	sheduler->addTask(task_broedmachine_run    , 0010, true);
	sheduler->addTask(task_broedmachine_run_ki , 1000, true);
	sheduler->addTask(task_broedmachine_run_dht, 2000, true);

}

void Incubator::run(){
	bool door_is_open = is_door_open();
	if( door_is_open && not flagged(FLAG_DOOR_OPEN)){
		flag(FLAG_DOOR_OPEN);
		flag(FLAG_PAUZE);
		r_light->turnOn();
	}else if( not door_is_open && flagged(FLAG_DOOR_OPEN)){
		unflag(FLAG_DOOR_OPEN);
		unflag(FLAG_PAUZE);
		r_light->turnOff();
	}

	door_is_open ? r_fan->turnOff() : r_fan->turnOn();
	
}

void Incubator::run_dth(){
	float temp,hum;
	static uint8_t read_errors, change_count;

	// Read temperature en humidity
	dhtsensor->readSensor();
	Serial.print("DHT: ");Serial.println(dhtsensor->getStatusString());

	// Check for reading error
	if( dhtsensor->getStatus() != DHT::ERROR_NONE ){
		// Count the error (but stop at 6 counts)
		if(read_errors < 6) read_errors += 1;
		// If there were 5 errors in a row
		if(read_errors == 5){
			// Set error flag
			flag(FLAG_DHT_ERROR);
			// Power off heating
			r_heating->turnOff();
			unflag( FLAG_HEATING_ON);
		}
		// Stop execution
		return;

	// If no error, but previous errors existed: reset counter and flag
	}else if(read_errors > 0){
		read_errors = 0;
		unflag(FLAG_DHT_ERROR);
	}

	if ( temp = dhtsensor->getTemperature(true), temp != status.temperature){
		status.temperature = temp;
		Serial.print("! temp=");Serial.println(status.temperature);
		flag(FLAG_CHANGE);
	}
	if ( hum = dhtsensor->getHumidity(true), hum != status.humidity){
		status.humidity = hum;
		
		// Sometimes the reading becomes corrupt/incomplete,
		// the DHT then returns a temperature and humidity of 1 or 0.
		// Check for this by checking if humidity lower than 5
		if ( hum < 5 ){ flag(FLAG_DHT_ERROR); }
		
		Serial.print("! humi=");Serial.println(status.humidity);
		flag(FLAG_CHANGE);
	}

	// If running without errors
	if (not flagged(FLAG_PAUZE) && not flagged(FLAG_DHT_ERROR)){

		// If temp is below min temp and the heating is not already on
		if ( status.temperature < config.min_temp && r_heating->isOff() ){
			r_heating->turnOn();
			flag(FLAG_HEATING_ON);

		// If temp is higher than min temp and the heating is on
		}else if( status.temperature >= config.min_temp && r_heating->isOn()) {
			r_heating->turnOff();
			unflag(FLAG_HEATING_ON);
		}
	// Turn heating off when paused and remove flag too
	}else{
		if(r_heating->isOn()){
			r_heating->turnOff();
			unflag(FLAG_HEATING_ON);
		}		
	}
}

void Incubator::run_ki(Task* self){
	static uint16_t ki_timer; // Timeout

	// Do Nothing if the PAUZE flag is up
	if( flagged(FLAG_PAUZE) ){
		if(not flagged(FLAG_KI_IDLE)) ki_stop();
		return;
	}

	// Count one tick (should be one second)
	ki_timer += 1;

	// If timeout is reached
	if (ki_timer == KI_INTERVAL){
		// Toggle up/down flag
		status.flags ^= FLAG_KI_POS;
		// Move in that direction
		ki_turn( status.flags & FLAG_KI_POS);
		// Shedule next run (will end up in the next else-if)
		self->idle_cycles = KI_TIMEOUT;// 
	}else if(ki_timer > KI_INTERVAL){
		// Done: stop KI
		ki_stop();
		// Reset timeout
		ki_timer = 0;
	}
}

void Incubator::flag( uint8_t flag ){
	status.flags |= flag;

	Serial.print("flagging: ");Serial.println(flag);


	// When KI direction is changed or incubator is stopped or paused, remove OK and ERR flag so something starts happening (again).
	if ( flag == FLAG_KI_POS || flag == FLAG_PAUZE ){
		ki_stop();
		r_heating->turnOff();
	}

	// if ( not (flag & FLAG_CHANGE) )
	status.flags |= FLAG_CHANGE;

	// Serial.print("Incubator status: ");
	// Serial.println( status.flags, BIN );
}
void Incubator::unflag( uint8_t flag ){
	status.flags &= ~flag;
	status.flags |= FLAG_CHANGE;
}

bool Incubator::flagged(uint8_t flag){
	return (bool) (status.flags & flag);
}


bool Incubator::ki_in_position( bool direction ){
	return digitalRead( flagged(FLAG_KI_POS) == DOWN ? i_ki_down : i_ki_up );
}
bool Incubator::is_door_open(){
	return digitalRead( i_door_open );
}

void Incubator::ki_stop(){
	r_ki_power->turnOff();
	r_ki_direction->turnOff();
	Serial.println("Ki power off");
	flag(FLAG_KI_IDLE);
}

void Incubator::ki_turn( bool direction ){
	Serial.print("KI is going ");Serial.println(direction ? "UP" : "DOWN");
	if ( direction == HIGH ){ // Up
		r_ki_direction->turnOn();
	}else{ // Down
		r_ki_direction->turnOff();
	}
	delay(10);
	r_ki_power->turnOn();
	Serial.println("Ki power on");

	unflag(FLAG_KI_IDLE);
}



