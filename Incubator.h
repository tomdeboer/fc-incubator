#ifndef __Incubator_h__
#define __Incubator_h__

#include <Relay.h>
#include <DHT.h>
#include <Sheduler.h>

#define FLAG_PAUZE      1
#define FLAG_KI_IDLE    2
#define FLAG_KI_POS     4
#define FLAG_DHT_ERROR  8
#define FLAG_HEATING_ON 16
#define FLAG_FAN_ON     32
#define FLAG_DOOR_OPEN  64
#define FLAG_CHANGE     128

#define UP              true
#define DOWN            false


#ifndef KI_INTERVAL
	#warning "KI_INTERVAL not set!"
	#define KI_INTERVAL 3600
#endif
#ifndef KI_TIMEOUT
	// #define KI_TIMEOUT 1333
	#define KI_TIMEOUT 2000
#endif


struct IncubatorConfig_t{
	float min_temp;
};

struct IncubatorStatus_t{
	uint8_t flags;
	float temperature;
	float humidity;
};


class Incubator{
	public:

		static Incubator* getInstance();

		IncubatorConfig_t config;
		IncubatorStatus_t status;

		void flag   ( uint8_t flag ); // Sets a flag.
		void unflag ( uint8_t flag ); // Sets a flag.
		bool flagged( uint8_t flag ); // Checks if a flag is set.

		void run();               // Run Incubator's program.
		void run_ki(Task* self ); // Run keerinrichting program.
		void run_dth( );          // Run temperature/humidity sensor program.

		void init(Relay* relay_heating, Relay* relay_fan, Relay* relay_light, Relay* relay_power, Relay* relay_direction, uint8_t up_sensor, uint8_t down_sensor, uint8_t door_sensor, DHT* dht, Sheduler* sheduler);

		bool is_door_open( );
	protected:

		Relay* r_ki_power; 
		Relay* r_ki_direction;
		Relay* r_heating;
		Relay* r_fan;
		Relay* r_light;

		DHT* dhtsensor;

		uint8_t last_status; // Status after last run. This is checked against when run to detect changes.

		uint8_t i_ki_up;    // Up-sensor pin
		uint8_t i_ki_down;  // Down-sensor pin
		uint8_t i_door_open;// Door open pin

		void ki_stop(); // Turns off both KI relays.
		void ki_turn( bool direction ); // Adjusts relays to turn KI in either direction.
		bool ki_in_position( bool direction ); // Checks if the KI is in either position.


};


#endif


