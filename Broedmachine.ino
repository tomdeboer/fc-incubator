#include <Relay.h>
#include <DHT.h>
#include <Sheduler.h>

#include <printf.h>            // Allows for printf on Arduino
#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>

#include "Incubator.h"
#include "../fc-ethernetproxy/communication.h"


#define ENABLE_RF       1
#define ENABLE_TIME     2
#define ENABLE_TEMP     4
#define ENABLE_EEPROM   8
#define ENABLE_COMMANDS 16

#define ENABLED ( ENABLE_TIME | ENABLE_TEMP | ENABLE_EEPROM | ENABLE_COMMANDS)


#define LED_PIN            13
#define BUFF_LEN           64 // Length of command buffer

// Outputs
#define KI_POWER_PIN       6
#define KI_DIRECTION_PIN   7
#define HEATER_PIN         2
#define FAN_PIN            3
#define LIGHT_PIN          5

// Inputs
#define KI_UP_SENSOR_PIN   A0
#define KI_DOWN_SENSOR_PIN A1
#define DOOR_OPEN_PIN      9
#define DHT_PIN            8

DHT dht; // Setup is done in setup();
#define MIN_TEMP           37.61
// #define MIN_TEMP           8.5

Incubator* broedmachine       = Incubator::getInstance();
	

void task_debug_outputs(Task* self){
	static const uint8_t pins[] = { KI_POWER_PIN, KI_DIRECTION_PIN, HEATER_PIN, FAN_PIN, KI_UP_SENSOR_PIN, DHT_PIN };

	for(int p = 0; p <= sizeof(pins); ++p ){
		if (p < 10 ) Serial.print(' ');
		Serial.print( pins[p] );
		Serial.print(' ');
		Serial.println( digitalRead( pins[p] ) ? "HIGH" : "LOW" );
	}
}

Sheduler sheduler;
RF24 radio(48,49); // CE-pin = 8, CSN-pin = 9
static uint8_t rf_buffer[ 24 ];

void setup_rf(){
	#if (ENABLED & ENABLE_RF)
		radio.begin();
		delay(1);
		radio.setPayloadSize(24);
		radio.setRetries(15,15); //Increase the delay between retries & # of retries. Max is 15/15.
		radio.setDataRate( RF24_250KBPS ) ;
		radio.setChannel(100);

		radio.openWritingPipe( NODE_ADDR( MASTER ) );  // Pipe to master
		radio.openReadingPipe( 1 , NODE_ADDR( INCUBATOR1 ) ); // Pipe to self

		radio.startListening();
	#endif
}
bool send_rf(uint64_t target_addr, event_e event, void* data, uint8_t len){
	#if (ENABLED & ENABLE_RF)
		radio.openWritingPipe( target_addr );  // Pipe to target

		memset(&rf_buffer, 0, sizeof(rf_buffer));

		// Set event
		rf_buffer[0] = event;
		// Copy data to rf_buffer
		if (len)
			memcpy(&rf_buffer[1], data, len);

		// printbuff(&rf_buffer[0], sizeof(rf_buffer));

		radio.stopListening();

		// Write to open writing pipe. Only one write pipe is ever open, so write to that one
		bool result = radio.write(&rf_buffer, sizeof(rf_buffer));

		radio.startListening();

		return result;
	#else
		return true;
	#endif
}
void task_propagate_changed(Task* self){
	static uint8_t errors;

	if ( not (broedmachine->status.flags & FLAG_CHANGE) ) return;
	Serial.print("! status="); Serial.println(broedmachine->status.flags);


	if ( send_rf( NODE_ADDR( MASTER ), inc_status, &broedmachine->status, sizeof(IncubatorStatus_t) ) ){
		broedmachine->status.flags &= ~FLAG_CHANGE;
		errors = 0;
		Serial.println("Master notified");
		self->idle_cycles = 100;
	}else{
		self->idle_cycles = 500;
		Serial.println("Error Reaching master");
		if(++errors >= 30){
			setup_rf();
		}
	}
}
void task_led(Task* self){
	if( digitalRead(LED_PIN) != broedmachine->flagged(FLAG_CHANGE)){
		digitalWrite(LED_PIN, broedmachine->flagged(FLAG_CHANGE));
	}
}

void setup_dht(){
	dht.setup(DHT_PIN);
}

void setup(){
	

	Serial.begin( 115200 );	// Debugging only
	printf_begin();
	// delay( 2000 );

	pinMode(LED_PIN, OUTPUT);

	setup_dht();

	broedmachine->config.min_temp = MIN_TEMP;
	broedmachine->init(
		new Relay( HEATER_PIN      , RELAY_NORMAL),
		new Relay( FAN_PIN         , RELAY_NORMAL),
		new Relay( LIGHT_PIN       , RELAY_NORMAL),
		new Relay( KI_POWER_PIN    , RELAY_NORMAL),
		new Relay( KI_DIRECTION_PIN, RELAY_NORMAL),
		KI_UP_SENSOR_PIN,
		KI_DOWN_SENSOR_PIN,
		DOOR_OPEN_PIN,
		&dht,
		&sheduler
	);

	// sheduler.addTask(task_debug_outputs   , 2000, false);
	setup_rf();
	// radio.printDetails();

	sheduler.addTask(task_led, 0, false);
	sheduler.addTask(task_propagate_changed, 0, false);

	Serial.println("Ready!");

	// Setup Timer1 for the sheduler,
	// Every 1 millisecond.
	TCCR1A  = 0x00;
	TCCR1B  = (1 << WGM12) | (1 << CS10) | (1 << CS11); // Set prescaler to 16.
	OCR1A   = 250;                                      // 16Mhz / 16 / 1000ms = 250 ticks.
	TIMSK1 |= (1 << OCIE1A);                            // Set the ISR COMPA vect.

	// Serial.println("Wait..");
	// delay(3000);
	// Serial.println("Run!");
}


#if (ENABLED & ENABLE_COMMANDS)
	char buf[ BUFF_LEN ];
	uint8_t pos = 0; // Position in buf;

	void parseCommand(char *_command ){

		 uint8_t flag, value;

		 Serial.print('\'');Serial.print(_command);Serial.println('\'');
		 
		 int s;
		 
		 if (sscanf(_command, "S %d", &flag) == 1 ){ // 3 variabelen nodig       Serial.print("Setting ");
			 Serial.print("Setting ");
			 Serial.println( flag );
			 delay(100);
			 // digitalWrite( flag , HIGH );
			 broedmachine->flag( flag );
			 
		 }else if (sscanf(_command, "R %d", &flag) == 1 ){
			 Serial.print("Retting ");
			 Serial.println( flag );
			 delay(100);
			 
			 // digitalWrite( flag , LOW );
			 broedmachine->unflag( flag );
		 }else{
			 Serial.println("Unknown command");   
		 }
	}
	void serialEvent(){
		while ( Serial.available() && pos < BUFF_LEN ){
			buf[ pos ] = Serial.read();

			if ( buf[ pos ] == '\n' ){
				buf[ pos ] = 0;
				parseCommand( (char*) &buf );
				// memset( buf, 0, sizeof(buf));
				pos = 0;
			}else{
				pos += 1;
			};
		}
	}
#endif;

unsigned long int last_ms = 0, ms = 0;
void loop(){
	sheduler.run();
	if( ms = millis(), (ms - last_ms) > 3000){
		if( broedmachine->flagged(FLAG_CHANGE) ){
			Serial.print("New status: "); Serial.println(broedmachine->status.flags, BIN);
			Serial.print("Temp:");        Serial.println(broedmachine->status.temperature);
			Serial.print("Humi:");        Serial.println(broedmachine->status.humidity);
		}
		if( broedmachine->flagged(FLAG_DHT_ERROR) ){
			setup_dht();
		}
		last_ms = ms;
	}
}


ISR(TIMER1_COMPA_vect) {
	sheduler.tick();
}

